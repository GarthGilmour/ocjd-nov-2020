package samples.streams.core.operators;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Program {

    public static void main(String[] args) {
        showForEachWrong();
        showForEachRight();
        showFilter();
        showMap();
        showReduce();
        showPrimitiveStreams();
        showTesting();
        showFlatMap();
        showCollect();
    }

    private static void showCollect() {
        printTitle("Using Collect To Partition");
        Map<Boolean, List<String>> result = Builders.builder1()
                .stream()
                .collect(Collectors.partitioningBy(s -> s.length() == 3));

        printTabbed("Strings of length three");
        result.get(true).forEach(Program::printTabbed);

        printTabbed("Strings NOT of length three");
        result.get(false).forEach(Program::printTabbed);
    }

    private static void showFlatMap() {
        printTitle("Show flatMap version 1");
        Builders.builder4().flatMap(x -> x)
                .filter(x -> x > 0.5)
                .forEach(Program::printTabbed);

        printTitle("Show flatMap version 2");
        Builders.builder4().flatMap(stream -> stream.filter(x -> x > 0.5))
                .forEach(Program::printTabbed);
    }

    private static void showTesting() {
        printTitle("Testing");

        boolean result1 = Builders.builder1().stream().allMatch(s -> s.length() == 3);  //every
        boolean result2 = Builders.builder1().stream().anyMatch(s -> s.length() == 3);  //some
        boolean result3 = Builders.builder1().stream().noneMatch(s -> s.length() == 3); //none

        printTabbed(result1);
        printTabbed(result2);
        printTabbed(result3);
    }

    private static void showPrimitiveStreams() {
        printTitle("Using Primitive Streams");
        double average = Builders.builder2().average().orElse(0.0);
        double max = Builders.builder2().max().orElse(0.0);
        double min = Builders.builder2().min().orElse(0.0);
        double sum = Builders.builder2().sum();

        printTabbed("Average is: "+ average);
        printTabbed("Max is: "+ max);
        printTabbed("Min is: "+ min);
        printTabbed("Sum is: "+ sum);
    }

    private static void showReduce() {
        printTitle("Using Reduce");
        int result1 = Builders.builder1().stream()
                .map(String::length)        //Stream<Integer>
                .reduce((a,b) -> a + b)
                .orElse(0);
        printTabbed(result1);

        printTitle("Avoiding Reduce");
        int result2 = Builders.builder1().stream()
                .mapToInt(String::length)  //IntStream
                .sum();
        printTabbed(result2);
    }

    private static void showMap() {
        printTitle("Map to object");
        Builders.builder1().stream()
                .map(String::length)            //Stream<Integer>
                .forEach(Program::printTabbed);

        printTitle("Map to primitive");
        Builders.builder1().stream()
                .mapToInt(String::length)       //IntStream
                .forEach(Program::printTabbed);
    }

    private static void showFilter() {
        printTitle("Filter");
        Builders.builder1().stream()
                .filter(s -> s.length() == 3)
                .forEach(Program::printTabbed);
    }

    private static void showForEachWrong() {
        printTitle("For each done wrong");
        Builders.builder1().stream().forEach(s -> printTabbed(s));
    }

    private static void showForEachRight() {
        printTitle("For each done right");
        Builders.builder1().forEach(Program::printTabbed);
    }

    private static void printTitle(String title) {
        System.out.printf("---- %s ----\n", title);
    }

    private static void printTabbed(Object thing) {
        System.out.printf("\t %s \n", thing);
    }
}
