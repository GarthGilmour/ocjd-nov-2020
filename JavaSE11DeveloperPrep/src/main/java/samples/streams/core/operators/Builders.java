package samples.streams.core.operators;

import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class Builders {
    static List<String> builder1() {
        return Arrays.asList(
                "ab", "cde", "fghi",
                "kl", "mno", "opqr",
                "st", "uvw", "xyz"
        );
    }

    static DoubleStream builder2() {
        return DoubleStream.generate(Math::random).limit(5);
    }

    static Stream<Double> builder3() {
        return Stream.generate(Math::random).limit(5);
    }

    static Stream<Stream<Double>> builder4() {
        return Stream.generate(Builders::builder3).limit(5);
    }
}
