package samples.creating.functional.interfaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

interface Checker {
    boolean check(String s);
}

interface Transformer {
    int transform(String s);
}

interface Accumulator {
    String accumulate(String first, String second);
}

public class Program {
    private static String myReduce1(List<String> input, Accumulator action) {
        String current = action.accumulate(input.get(0), input.get(1));
        for(int i = 2; i < input.size(); i++) {
            current = action.accumulate(current, input.get(i));
        }
        return current;
    }

    private static String myReduce2(List<String> input, BiFunction<String, String, String> action) {
        String current = action.apply(input.get(0), input.get(1));
        for(int i = 2; i < input.size(); i++) {
            current = action.apply(current, input.get(i));
        }
        return current;
    }

    private static <T> T myReduce3(List<T> input, BiFunction<T, T, T> action) {
        T current = action.apply(input.get(0), input.get(1));
        for(int i = 2; i < input.size(); i++) {
            current = action.apply(current, input.get(i));
        }
        return current;
    }

    private static List<Integer> myMap1(List<String> input, Transformer action) {
        List<Integer> output = new ArrayList<>();
        for(String str : input) {
            output.add(action.transform(str));
        }
        return output;
    }

    private static List<Integer> myMap2(List<String> input, Function<String, Integer> action) {
        List<Integer> output = new ArrayList<>();
        for(String str : input) {
            output.add(action.apply(str));
        }
        return output;
    }

    private static <T, R> List<R> myMap3(List<T> input, Function<T, R> action) {
        List<R> output = new ArrayList<>();
        for(T item : input) {
            output.add(action.apply(item));
        }
        return output;
    }

    private static List<String> myFilter1(List<String> input, Checker action) {
        List<String> output = new ArrayList<>();
        for(String str : input) {
            if(action.check(str)) {
                output.add(str);
            }
        }
        return output;
    }

    private static List<String> myFilter2(List<String> input, Predicate<String> action) {
        List<String> output = new ArrayList<>();
        for(String str : input) {
            if(action.test(str)) {
                output.add(str);
            }
        }
        return output;
    }

    private static <T> List<T> myFilter3(List<T> input, Predicate<T> action) {
        List<T> output = new ArrayList<>();
        for(T item : input) {
            if(action.test(item)) {
                output.add(item);
            }
        }
        return output;
    }

    public static void main(String[] args) {
        List<String> data = Arrays.asList("ab","cde","fg","hij");

        showFiltering(data);
        showMapping(data);
        showReducing(data);
    }

    private static void showReducing(List<String> data) {
        System.out.println("First reduce results");
        String result1 = myReduce1(data, (a, b) -> a + b);
        System.out.println(result1);

        System.out.println("Second reduce results");
        String result2 = myReduce2(data, (a, b) -> a + b);
        System.out.println(result2);

        System.out.println("Third reduce results");
        String result3 = myReduce3(data, (a, b) -> a + b);
        System.out.println(result3);
    }

    private static void showMapping(List<String> data) {
        System.out.println("First map results");
        List<Integer> results1 = myMap1(data, String::length);
        results1.forEach(System.out::println);

        System.out.println("Second map results");
        List<Integer> results2 = myMap2(data, String::length);
        results2.forEach(System.out::println);

        System.out.println("Third map results");
        List<Integer> results3 = myMap3(data, String::length);
        results3.forEach(System.out::println);
    }

    private static void showFiltering(List<String> data) {
        System.out.println("First filter results");
        List<String> results1 = myFilter1(data, s -> s.length() == 3);
        results1.forEach(System.out::println);

        System.out.println("Second filter results");
        List<String> results2 = myFilter2(data, s -> s.length() == 2);
        results2.forEach(System.out::println);

        System.out.println("Third filter results");
        List<String> results3 = myFilter3(data, s -> s.length() == 2);
        results3.forEach(System.out::println);
    }
}
