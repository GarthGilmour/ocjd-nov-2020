package questions.Q8;

import java.util.function.*;

public class Program {
	public static void main(String[] args) {
		Consumer<String> f1 = System.out::println;
		f1.accept("Hello Java 8");
	}
}
