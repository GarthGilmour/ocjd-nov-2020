package questions.Q31;

enum Colour {
    RED,
    GREEN,
    BLUE,
    Blue //Not a good idea
}

public class Program {
    public static void main(String[] args) {
        String str1 = "BLUE";
        Colour colour1 = Colour.valueOf(str1);
        String str2 = "Blue";
        Colour colour2 = Colour.valueOf(str2);
        System.out.println(colour1);
        System.out.println(colour2);

        try {
            Colour.valueOf("blue");
        } catch(IllegalArgumentException ex) {
            System.out.println("Conversion failed because: " + ex.getMessage());
        }
    }
}
