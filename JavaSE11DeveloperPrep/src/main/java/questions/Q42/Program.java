package questions.Q42;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.time.LocalDate.ofYearDay;

public class Program {
    public static void main(String[] args) {
        List<String> data1 = List.of("Fred", "Wilma", "Barney", "Betty");
        data1.forEach(System.out::println);

        Set<String> data2 = Set.of("Fred", "Wilma", "Barney", "Betty");
        data2.forEach(System.out::println);

        Map<String, LocalDate> data3 = Map.of(
                "Moonlight", ofYearDay(2017, 1),
                "Spotlight", ofYearDay(2016, 1),
                "Birdman", ofYearDay(2015, 1),
                "12 Years a Slave", ofYearDay(2014, 1),
                "Argo", ofYearDay(2013, 1));
        data3.entrySet().forEach(System.out::println);

        Map<String, LocalDate> data4 = Map.ofEntries(
                Map.entry("Moonlight", ofYearDay(2017, 1)),
                Map.entry("Spotlight", ofYearDay(2016, 1)),
                Map.entry("Birdman", ofYearDay(2015, 1)),
                Map.entry("12 Years a Slave", ofYearDay(2014, 1)),
                Map.entry("Argo", ofYearDay(2013, 1)));
        data4.entrySet().forEach(System.out::println);
    }
}
