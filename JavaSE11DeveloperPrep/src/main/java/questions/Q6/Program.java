package questions.Q6;

import java.util.Optional;

public class Program {
	public static Optional<String> readPropertyValue(String propName) {
		String value = System.getProperty(propName);
		if(value == null) {
			return Optional.empty();
		} 
		return Optional.of(value);
	}
	public static void main(String[] args) {
		System.out.println(readPropertyValue("java.version").orElse("unknown"));
		System.out.println(readPropertyValue("foo.bar").orElse("unknown"));
	}
}
