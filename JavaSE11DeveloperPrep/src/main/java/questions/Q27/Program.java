package questions.Q27;

import java.util.Date;

public class Program {
    public static void main(String[] args) {
        Demo<Date, Employee> demo1 = new Demo<>();
        demo1.func(new Date());
        demo1.func(new Employee());
        demo1.func(new Manager());
        demo1.func("abcdef");

        Demo<Date, Manager> demo2 = new Demo<>();
        demo2.func(new Date());
        //demo2.func(new Employee()); //Will not compile
        //demo2.func(new Manager());  //Will not compile
        demo2.func("abcdef");
    }
}
