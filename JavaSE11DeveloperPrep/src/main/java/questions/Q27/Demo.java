package questions.Q27;

class Demo<T, U extends Employee> {
    public void func(T param) {
        System.out.println("func(T)");
    }
    public void func(U param) {
        System.out.println("func(U)");
    }
    public void func(String param) {
        System.out.println("func(String)");
    }
    public void func(Manager param) {
        System.out.println("func(Manager)");
    }
}
