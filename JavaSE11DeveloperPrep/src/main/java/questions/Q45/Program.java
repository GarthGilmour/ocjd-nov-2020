package questions.Q45;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

public class Program {
    public static void main(String[] args) {
        LocalDate data1 = LocalDate.now(ZoneId.of("Europe/Paris"));
        LocalTime data2 = LocalTime.now(ZoneId.of("Europe/Paris"));
        LocalDateTime data3 = LocalDateTime.now(ZoneId.of("Europe/Paris"));

        System.out.println(data1);
        System.out.println(data2);
        System.out.println(data3);
    }
}
