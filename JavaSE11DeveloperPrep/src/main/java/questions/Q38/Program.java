package questions.Q38;

import java.util.*;
import java.util.stream.*;

public class Program {
	public static void main(String[] args) {
		List<String> list = Arrays.asList("abc","de","fgh","ij","klm","no","pqr");
		Stream<Integer> results1 = list.stream().map(x -> x.length());
		IntStream results2 = list.stream().mapToInt(x -> x.length());
		
		results1.forEach(System.out::println);
		System.out.println("----");
		results2.forEach(System.out::println);
	}
}
