package questions.Q70;

public class Program {
    public static void main(String[] args) {
        String str1 = new @Foo String("abc");
        String str2 = new String("def");

        test(str1); //A
        test(str2); //B
    }
    private static void test(Object input) {
        String result = (@Foo String) input;
        System.out.println(input);
    }
}
