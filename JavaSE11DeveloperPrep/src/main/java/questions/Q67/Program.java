package questions.Q67;

public class Program {
    public static void main(String[] args) {
        Pair<String,Character> tst1 = new Pair<>("ab", 'x');
        Pair<String,Character> tst2 = new Pair<>("cd", 'y');
        Pair<String,Character> tst3 = new Pair<>("ef", 'x');

        boolean r1 = isLarger(tst1,tst2);
        boolean r2 = isLarger(tst2,tst1);
        boolean r3 = isLarger(tst2,tst3);

        System.out.printf("Results are %b, %b and %b\n",r1,r2,r3);
    }

    private static <T extends Comparable<T>, U extends Comparable<U>> boolean isLarger(Pair<T,U> p1, Pair<T,U> p2) {
        if(p1.getFirst().compareTo(p2.getFirst()) > 0) {
            return p1.getSecond().compareTo(p2.getSecond()) > 0;
        }
        return false;
    }
}
