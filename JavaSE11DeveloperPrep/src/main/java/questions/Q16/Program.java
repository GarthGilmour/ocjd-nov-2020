package questions.Q16;

import java.util.*;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

public class Program {
	public static void main(String[] args) {
		List<String> input = Arrays.asList("abc","defg","hij","klmn");
		Predicate<String> test = s -> s.length() == 3;
		
		option1(input, test);
		System.out.println("---");
		option2(input, test);
	}
	private static void option1(List<String> input, Predicate<String> test) {
		List<String> output = new ArrayList<String>();
		input.stream()
			 .filter(test)
			 .forEach(s -> output.add(s));
		for(String s : output) {
			System.out.println(s);
		}
	}
	private static void option2(List<String> input, Predicate<String> test) {
		List<String> output = input.stream()
								    .filter(test)
								    .collect(toList());
		for(String s : output) {
			System.out.println(s);
		}
	}
}
