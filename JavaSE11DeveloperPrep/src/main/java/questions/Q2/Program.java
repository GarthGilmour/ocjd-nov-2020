package questions.Q2;

import java.util.function.*;

public class Program {
	public static void main(String[] args) {
		Function<Integer, Double> f1 = (a) -> a * 2.0;				//A
		//Function<Integer, Double> f2 = (int a) -> a * 2.0;		//B
		Function<Integer, Double> f3 = (Integer a) -> a * 2.0;		//C
		IntToDoubleFunction f4 = (a) -> a * 2.0;					//D
		IntToDoubleFunction f5 = (int a) -> a * 2.0;				//E
		//IntToDoubleFunction f6 = (Integer a) -> a * 2.0;			//F
	}

}
