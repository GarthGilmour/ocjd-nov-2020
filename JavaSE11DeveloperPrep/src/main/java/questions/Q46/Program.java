package questions.Q46;

import java.util.ArrayDeque;
import java.util.Queue;

public class Program {
    public static void main(String[] args) {
        ArrayDeque<String> data = new ArrayDeque<>();
        String output = test(data);
        System.out.println(output);
    }

    private static String test(Queue<String> input) {
        return input.poll();
    }
}
