package questions.Q54;

import java.util.ArrayDeque;
import java.util.Queue;

public class Program {
    public static void main(String[] args) {
        ArrayDeque<String> data = new ArrayDeque<>();
        System.out.println(data.peekFirst());   //A
        System.out.println(data.getFirst());    //B
    }
}
