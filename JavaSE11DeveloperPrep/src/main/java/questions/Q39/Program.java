package questions.Q39;

import java.util.*;
import java.util.function.Predicate;

public class Program {
	public static void main(String[] args) {
		List<String> list = Arrays.asList("abc","de","fgh","ij","klm","no","pqr");
		Predicate<String> f = x -> x.length() == 3;
		System.out.println(list.stream().noneMatch(f));
		System.out.println(list.stream().anyMatch(f));
		System.out.println(list.stream().allMatch(f));
	}
}
