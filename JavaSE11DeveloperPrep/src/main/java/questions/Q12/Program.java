package questions.Q12;

import java.util.Arrays;
import java.util.List;

public class Program {

	public static void main(String[] args) {
		List<String> data = Arrays.asList("foo","bar","zed");
		data.forEach(s -> System.out.println(s));
		System.out.println("----");
		data.stream().forEach(s -> System.out.println(s));
		System.out.println("----");
		data.forEach(System.out::println);
		System.out.println("----");
		data.stream().forEach(System.out::println);
	}

}
