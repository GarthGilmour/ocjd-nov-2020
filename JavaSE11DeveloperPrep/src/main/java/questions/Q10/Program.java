package questions.Q10;

import java.util.*;

public class Program {
	public static <E, T extends List<E>> void printFirst(T input) {
		E first = input.get(0);
		System.out.println(first.toString());
	}
	public static void main(String[] args) {
		LinkedList<String> list1 = new LinkedList<>();
		list1.add("abc");
		printFirst(list1);
	}
}
