package questions.Q33;

class Employee {}
class Manager extends Employee {}

public class Demo {
    public static void main(String [] args) {
        Base b = new Derived();
        b.func1(new Manager());
        b.func2(new Manager());
    }
}

class Base {
    public <T extends Employee> void func1(T param) {
        System.out.println("Base.func1");
    }
    public void func2(Employee param) {
        System.out.println("Base.func2");
    }
}

class Derived extends Base {
    public void func1(Employee param) {
        System.out.println("Derived.func1");
    }
//    public <T extends Employee> void func2(T param) {
//        System.out.println("Derived.func2");
//    }
}
