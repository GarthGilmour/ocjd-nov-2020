package questions.Q43;

public class Program {
    public static void main(String[] args) {
        try (MyResource r = new MyResource()) {
            r.action();
        }
    }
}
