package questions.Q56;

public class Program {
    public static void main(String[] args) {
        @Foo Accumulator<Integer> a1 = new IntegerAccumulator(5);
        Accumulator<Integer> a2 = new @Foo IntegerAccumulator(5);
        @Foo Accumulator<Integer> a3 = new @Foo IntegerAccumulator(5);
        //Accumulator<Integer> @Foo a4 = new @Foo IntegerAccumulator(5);

        System.out.println(a1.add(3));
        System.out.println(a2.add(6));
        System.out.println(a3.add(9));
    }
}
