package questions.Q56;

public interface Accumulator<T> {
    T add(T increment);
}
