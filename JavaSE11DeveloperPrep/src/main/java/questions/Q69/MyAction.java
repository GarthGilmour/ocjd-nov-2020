package questions.Q69;

import java.security.PrivilegedAction;

public class MyAction implements PrivilegedAction<String> {
    private String name;

    public MyAction(String name) {
        this.name = name;
    }

    @Override
    public String run() {
        return System.getProperty(name);
    }
}
