package questions.Q69;

import java.security.AccessController;

public class Program {
    public static void main(String[] args) {
        String result = AccessController.doPrivileged(new MyAction("java.vendor"));
        System.out.println(result);
    }
}
