package questions.Q47;

public class Program {
    public static void main(String[] args) {
        System.out.println(buildQuery("AB12"));
    }
    static String buildQuery(String uiInput) {
        var query = "SELECT c FROM Customer WHERE c.custID = '%s'";
        return String.format(query, uiInput);
    }
}
