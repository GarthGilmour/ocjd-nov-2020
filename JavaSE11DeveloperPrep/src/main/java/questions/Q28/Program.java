package questions.Q28;

import java.io.*;
import java.nio.file.*;

public class Program {
	public static void main(String[] args) throws IOException {
		final String pathName = "./exam/OracleJSE11DeveloperSampleExam.html";
		final Path filePath = new File(pathName).toPath();
		Files.lines(filePath)
		     .forEach(System.out::println);
	}
}
