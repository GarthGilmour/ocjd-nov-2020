package questions.Q5;

class MyClass {
	public static void main(String[] args) {
		Inner i;
		i = new MyClass().new Inner(); //missing line goes here
		i.func();
	}
	private class Inner {
		void func() {
			System.out.println("Hello From Inner");
		}
	}
}
