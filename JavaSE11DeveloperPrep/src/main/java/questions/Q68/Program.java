package questions.Q68;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Program {
    private static final ThreadLocal<String> state = new ThreadLocal<>();
    private static final ExecutorService service = Executors.newSingleThreadExecutor();

    public static void main(String[] args) {
        state.set("foo");
        service.submit(() -> {
            state.set("bar");
            printOutput();
        });
        printOutput();
    }
    private static void printOutput() {
        System.out.println(state.get());
    }
}
