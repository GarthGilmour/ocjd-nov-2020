package questions.Q49;

import java.time.LocalDate;

public class Program {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        LocalDate endOfYear = now.withDayOfMonth(31).withMonth(12);
        System.out.println(endOfYear);
    }
}
