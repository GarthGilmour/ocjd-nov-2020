package questions.Q58;

import java.io.IOException;

public class Program {
    public static void main(String[] args) {
        try {
            test();
        } catch (IOException | InterruptedException ex) {
            System.out.println("Caught error ot type " + ex.getClass().getSimpleName());
        }
    }

    private static void test() throws IOException, InterruptedException {
        double value = Math.random();
        if(value < 0.33) {
            throw new IOException();
        } else if(value < 0.66) {
            throw new InterruptedException();
        }
    }
}
