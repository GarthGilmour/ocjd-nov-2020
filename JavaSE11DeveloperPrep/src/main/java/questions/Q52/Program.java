package questions.Q52;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Program {
    public static void main(String[] args) throws Exception {
        var result = onRequest("qwerty");
        System.out.println(result.get());
    }

    static Future<String> onRequest(String input) {
        return Executors.newSingleThreadExecutor()
                .submit(() -> doWork(input));
    }

    private static String doWork(String input) {
        return "Done " + input;
    }
}
