# Java Certification Workshop #

This repository contains materials to help prepare for the 'Java SE 11 Developer Certification' exam. The folder 
entitled 'JavaSE11DeveloperPrep' is formatted as a Gradle project, to be opened in IntelliJ or Eclipse. It's contents 
are as follows:

* The 'exam' folder holds a sample exam with 75 questions. Please attempt these in advance of the workshop.
* The subpackages under 'questions' contain code to verify the Java snippets used in the exam.
* The subpackages under 'samples' contain more in depth examples of the topics being tested.
* The subpackages under 'exercises' contain practical work we may attempt.
